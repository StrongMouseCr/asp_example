﻿using ASP_Example.Models.DbModels;
using ASP_Example.Models;
using Microsoft.EntityFrameworkCore;

namespace ASP_Example;

public class AppContext : DbContext
{
    public DbSet<Project> Projects { get; set; }
    public DbSet<Employee> Employees { get; set; }

    public AppContext(DbContextOptions<AppContext> options) : base(options)
    {
        Database.EnsureCreated();
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Project>()
            .HasMany(project => project.Employees)
            .WithMany(employee => employee.Projects)
            .UsingEntity(x => x.ToTable("ProjectsEmployees"));

        modelBuilder.Entity<Project>()
            .HasOne(project => project.Supervisor)
            .WithMany()
            .HasForeignKey("SupervisorForeignKey")
            .OnDelete(DeleteBehavior.SetNull);
    }
}