﻿using System.ComponentModel.DataAnnotations;
using ASP_Example.Infrastructure;
using ASP_Example.Models.CRUDs;
using ASP_Example.Models.DbModels;
using ASP_Example.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ASP_Example.Controllers;

[ApiController]
[Route("employee")]
[CustomExceptionFilter]
public class EmployeeController : ControllerBase
{
    private readonly EmployeeCrud _employeeCrud;

    public EmployeeController(EmployeeCrud employeeCrud)
    {
        _employeeCrud = employeeCrud;
    }

    [HttpGet]
    public IActionResult Get([FromQuery] int? id)
    {
        return new ContentResult
        {
            Content = JsonConvert.SerializeObject(_employeeCrud.Get(id), new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.None
            }),
            ContentType = "application/json"
        };;
    }
    
    [HttpPut]
    public IActionResult Update([FromQuery][Required] int id, [FromBody][Required] Employee employee)
    {
        employee.Id = id;
        var updatedProject = _employeeCrud.Update(employee);
        return new ContentResult
        {
            Content = JsonConvert.SerializeObject(updatedProject, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.None
            }),
            ContentType = "application/json"
        };
    }
    
    [HttpPost]
    public IActionResult Add([FromBody] Employee employee)
    {
        _employeeCrud.Add(employee);
        return NoContent();
    }
    
    [HttpDelete]
    public IActionResult Delete([FromQuery][Required] int id)
    {
        _employeeCrud.Delete(id);
        return NoContent();
    }
}