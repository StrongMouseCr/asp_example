﻿using System.ComponentModel.DataAnnotations;
using ASP_Example.Infrastructure;
using ASP_Example.Models.CRUDs;
using ASP_Example.Models.DbModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ASP_Example.Controllers;

[ApiController]
[Route("project")]
[CustomExceptionFilter]
public class ProjectController : ControllerBase
{
    private readonly ProjectCrud _projectCrud;

    public ProjectController(ProjectCrud projectCrud)
    {
        _projectCrud = projectCrud;
    }

    [HttpGet]
    public IActionResult Get([FromQuery] int? id)
    {
        return new ContentResult
        {
            Content = JsonConvert.SerializeObject(_projectCrud.Get(id), new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.None
            }),
            ContentType = "application/json"
        };
    }
    
    [HttpPut]
    public IActionResult Update([FromQuery][Required] int id, [FromBody] Project project)
    {
        project.Id = id;
        var updatedProject = _projectCrud.Update(project);
        return new ContentResult
        {
            Content = JsonConvert.SerializeObject(updatedProject, new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = PreserveReferencesHandling.None
            }),
            ContentType = "application/json"
        };
    }
    
    [HttpPost]
    [Route("employees")]
    public IActionResult AddEmployee([FromQuery][Required] int idProject, [FromQuery][Required] int idEmployee)
    {
        _projectCrud.AddEmployee(idProject, idEmployee);
        return NoContent();
    }
    
    [HttpDelete]
    [Route("employees")]
    public IActionResult RemoveEmployee([FromQuery][Required] int idProject, [FromQuery][Required] int idEmployee)
    {
        _projectCrud.RemoveEmployee(idProject, idEmployee);
        return NoContent();
    }
    
    [HttpPut]
    [Route("supervisor")]
    public IActionResult ChangeSupervisor([FromQuery][Required] int idProject, [FromQuery][Required] int idSupervisor)
    {
        _projectCrud.ChangeSupervisor(idProject, idSupervisor);
        return NoContent();
    }
    
    [HttpPost]
    public IActionResult Add([FromBody] Project project)
    {
        _projectCrud.Add(project);
        return NoContent();
    }
    
    [HttpDelete]
    public IActionResult Delete([FromQuery][Required] int id)
    {
        _projectCrud.Delete(id);
        return NoContent();
    }
}