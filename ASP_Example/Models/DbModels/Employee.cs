﻿
namespace ASP_Example.Models.DbModels;

public class Employee
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string Surname { get; set; } = string.Empty;
    public string Patronymic { get; set; } = string.Empty;
    public string Email { get; set; } = string.Empty;

    public virtual List<Project> Projects { get; set; } = new();
}