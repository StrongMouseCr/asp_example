﻿namespace ASP_Example.Models.DbModels;

public class Project
{
    public int Id { get; set; }
    public string Name { get; set; } = string.Empty;
    public string CustomerCompany { get; set; } = string.Empty;
    public string ExecutorCompany { get; set; } = string.Empty;
    public DateTime StartTime { get; set; }
    public DateTime EndTime { get; set; }
    public int Priority { get; set; }
    
    public virtual Employee? Supervisor { get; set; }
    public virtual List<Employee> Employees { get; set; } = new();
}