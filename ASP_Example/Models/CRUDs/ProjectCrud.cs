﻿using ASP_Example.Models.DbModels;
using ASP_Example.Models.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace ASP_Example.Models.CRUDs;

public class ProjectCrud : ICrud<Project>
{
    private readonly AppContext _dbContext;

    public ProjectCrud(AppContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public void AddEmployee(int idProject, int idEmployee)
    {
        var project = _dbContext.Projects.Include(p => p.Employees).FirstOrDefault(p => p.Id == idProject);
        if (project is not null)
        {
            var employee = _dbContext.Employees.FirstOrDefault(e => e.Id == idEmployee);
            if (employee is not null)
            {
                project.Employees.Add(employee);
                _dbContext.Projects.Update(project);
                _dbContext.SaveChanges();
            }
            else
            {
                throw new Exception("Error in add employee");
            }
        }
        else
        {
            throw new Exception("Error in add employee");
        }
    }
    
    public void RemoveEmployee(int idProject, int idEmployee)
    {
        var project = _dbContext.Projects.Include(p => p.Employees).FirstOrDefault(p => p.Id == idProject);
        if (project is not null)
        {
            var employee = _dbContext.Employees.FirstOrDefault(e => e.Id == idEmployee);
            if (employee is not null)
            {
                project.Employees.Remove(employee);
                _dbContext.Projects.Update(project);
                _dbContext.SaveChanges();
            }
            else
            {
                throw new Exception("Error in remove employee");
            }
        }
        else
        {
            throw new Exception("Error in remove employee");
        }
    }

    public void ChangeSupervisor(int idProject, int idSupervisor)
    {
        var project = _dbContext.Projects.Include(p => p.Supervisor).FirstOrDefault(e => e.Id == idProject);
        if (project is not null)
        {
            var supervisor = _dbContext.Employees.FirstOrDefault(e => e.Id == idSupervisor);
            if (supervisor is not null)
            {
                project.Supervisor = supervisor;
                _dbContext.Projects.Update(project);
                _dbContext.SaveChanges();
            }
            else
            {
                throw new Exception("Error in change supervisor");
            }
        }
        else
        {
            throw new Exception("Error in change supervisor");
        }
    }

    public void Add(Project record)
    {
        _dbContext.Projects.Add(record);
        _dbContext.SaveChanges();
    }

    public ICollection<Project>? Get(int? id = null)
    {
        if (id == null) 
            return _dbContext.Projects.Include(p => p.Employees).ToList();
        var project = _dbContext.Projects.Include(p => p.Employees).FirstOrDefault(e => e.Id == id);
        return project is not null ? new List<Project> {project} : null;
    }

    public Project? Update(Project record)
    {
        var project = _dbContext.Projects.Include(p => p.Employees).FirstOrDefault(e => e.Id == record.Id);
        if (project is not null)
        {
            project.Name = record.Name;
            project.CustomerCompany = record.CustomerCompany;
            project.ExecutorCompany = record.ExecutorCompany;
            project.StartTime = record.StartTime;
            project.EndTime = record.EndTime;
            project.Priority = record.Priority;
            project.Supervisor = record.Supervisor;
            project.Employees = record.Employees;
            
            _dbContext.Projects.Update(project);
            _dbContext.SaveChanges();
        }
        else
        {
            throw new Exception("Error in update projects");
        }

        return project;
    }

    public void Delete(int id)
    {
        var project = _dbContext.Projects.FirstOrDefault(e => e.Id == id);
        if (project is not null)
        {
            _dbContext.Projects.Remove(project);
            _dbContext.SaveChanges();
        }
        else
        {
            throw new Exception("Error in delete project");
        }
    }
}