﻿using ASP_Example.Models.DbModels;
using ASP_Example.Models.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace ASP_Example.Models.CRUDs;

public class EmployeeCrud : ICrud<Employee>
{
    private readonly AppContext _dbContext;

    public EmployeeCrud(AppContext dbContext)
    {
        _dbContext = dbContext;
    }

    public void Add(Employee record)
    {
        _dbContext.Employees.Add(record);
        _dbContext.SaveChanges();
    }

    public ICollection<Employee>? Get(int? id = null)
    {
        if (id == null) 
            return _dbContext.Employees.Include(e => e.Projects).ToList();
        var project = _dbContext.Employees.Include(e => e.Projects).FirstOrDefault(e => e.Id == id);
        return project != null ? new List<Employee> {project} : null;
    }

    public Employee? Update(Employee record)
    {
        var project = _dbContext.Employees.Include(e => e.Projects).FirstOrDefault(e => e.Id == record.Id);
        if (project != null)
        {
            project.Name = record.Name;
            project.Surname = record.Surname;
            project.Patronymic = record.Patronymic;
            project.Email = record.Email;

            _dbContext.Employees.Update(project);
            _dbContext.SaveChanges();
        }
        else
        {
            throw new Exception("Error in update employees");
        }

        return project;
    }

    public void Delete(int id)
    {
        var project = _dbContext.Employees.FirstOrDefault(e => e.Id == id);
        if (project != null)
        {
            _dbContext.Employees.Remove(project);
            _dbContext.SaveChanges();
        }
        else
        {
            throw new Exception("Error in delete employee");
        }
    }
}