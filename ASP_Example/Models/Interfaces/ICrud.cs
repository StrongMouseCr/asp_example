﻿namespace ASP_Example.Models.Interfaces;

public interface ICrud<T>
{
    public void Add(T record);
    public ICollection<T>? Get(int? id = null);
    public T? Update(T record);
    public void Delete(int id);
}