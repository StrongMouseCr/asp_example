﻿namespace ASP_Example.Models;

public enum TaskStatus
{
    ToDo,
    InProgress,
    Done
}