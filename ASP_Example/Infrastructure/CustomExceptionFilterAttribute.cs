﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace ASP_Example.Infrastructure;

public class CustomExceptionFilterAttribute :  Attribute, IExceptionFilter
{
    public void OnException(ExceptionContext context)
    {
        context.Result = new BadRequestResult();
        context.ExceptionHandled = true;
    }
}